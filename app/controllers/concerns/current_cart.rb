#Concerns explanaition: https://stackoverflow.com/questions/14541823/how-to-use-concerns-in-rails-4
module CurrentCart
	extend ActiveSupport::Concern

	private

		def set_cart
			@cart = Cart.find(session[:cart_id])
		rescue ActiveRecord::RecordNotFound
			@cart = Cart.create
			session[:cart_id] = @cart.id
		end
	end